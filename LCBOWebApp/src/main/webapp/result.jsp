<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<style>
h1 {
	font-family: verdana;
}

p {
	font-family: courier;
}
</style>
<body class="page-header">
	<center>
		<h1>Available Brands</h1>
		<%
			List result = (List) request.getAttribute("brands");
			Iterator it = result.iterator();
			out.println("<br>At LCBO, We have <br><br>");
			while (it.hasNext()) {
				out.println(it.next() + "<br>");
			}
		%>
		<a href="index.html">Back to Main</a>
</body>
</html>